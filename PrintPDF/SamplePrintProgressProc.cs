﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Datalogics.PDFL;

namespace PrintPDF
{
    class SamplePrintProgressProc : PrintProgressProc
    {
        /// <summary>
        /// The method implements a callback (it MUST be named "Call" and exhibit the method signature described)
        /// It will be driven by DLE and provide data that can be used to update a progress bar, etc.
        /// </summary>
        /// <param name="page">The current page number. It is *always* 0-based. One *will* encounter a value of -1. That means "not applicable".</param>
        /// <param name="totalPages">The total number of pages printing. One *may* encounter a value of -1. That means "not applicable".</param>
        /// <param name="stagePercent">A percentage complete (of the stage). Values will always be in the range of 0.0 (0%) to 1.0 (100%)</param>
        /// <param name="info">A string that will present optional information that may be written to user interface</param>
        /// <param name="stage">An enumeration that indicates the current printing stage</param>
        public override void Call(int page, int totalPages, float stagePercent, string info, PrintProgressStage stage)
        {
            mSomeBoolean = ((mSomeBoolean) ? false : true);
            Logger.Log("Print Progress Proc was called.");

            //
            // Stage Information (info) is a *English language only* stage information string (e.g., font name)
            // suitable for use as (pre-localized) source material for a text label related to the current stage.
            // Most stages do not present any stage information (it's never null but *often* empty). For those
            // conditions one must create their own string value(s) to represent the stage name.
            // Stage (stage) is an enumeration of the stage (type). Switch on this value (always) to
            // vary the user presentation of any stage progress bar/info.
            //
            // For more detail (and inspiration) consult the PrintPDFGUI sample. The SamplePrintProgressProc
            // class (there) has a much richer implementation.
            //

            switch (stage)
            {
                case PrintProgressStage.PrintPage:
                    if (stagePercent < 1F)
                    {
                        Logger.Log(String.Format("Printing Page {0} of {1}", page + 1 /* 0 to 1-based */, totalPages));
                    }
                    break;
            }
        }

        static private bool mSomeBoolean;
    }
}
