﻿using System;
using System.IO;
using System.ServiceProcess;
using Datalogics.PDFL;

namespace PrintPDF
{
    partial class Service1 : ServiceBase
    {
        const string publicFolderPath = @"C:\Users\Public\RamBase\printpdf\";
        FileInfo finfo = null;

        public Service1()
        {
            InitializeComponent();
        }

        public void OnDebug()
        {
            OnStart(null);
        }

        protected override void OnStart(string[] args)
        {
            Logger.Log("Starting Service");

            string printer = string.Empty;
            string pdfFile = string.Empty;
            int noprints = 1;

            string configFilePath = publicFolderPath + "print.txt";
            Logger.Log("Config file path: " + configFilePath);

            try
            {
                using (StreamReader reader = new StreamReader(configFilePath))
                {
                    printer = reader.ReadLine();
                    pdfFile = reader.ReadLine();

                    string tempnoprints = reader.ReadLine();
                    if (!string.IsNullOrEmpty(tempnoprints))
                        noprints = int.Parse(tempnoprints);

                    Logger.Log("PDF filename: " + pdfFile);
                }

                if (string.IsNullOrEmpty(pdfFile))
                {
                    Logger.Log("Configure printer name and PDF file path in each line in the print.txt file");
                    return;
                }

                if (string.IsNullOrEmpty(pdfFile))
                    pdfFile = @"C:\Users\Public\RamBase\test.pdf";
            }
            catch (Exception ex)
            {
                Logger.Log(ex.Message + " " + ex.StackTrace);
            }

            finfo = new FileInfo(pdfFile);

            SendPrintJobToPrinter(finfo, printer, noprints);

        }

        protected override void OnStop()
        {
            Logger.Log("Stoping Service");
            // TODO: Add code here to perform any tear-down necessary to stop your service.
        }

        private static void SendPrintJobToPrinter(FileInfo finfo, string printer, int NCopies)
        {
            try
            {
                using (Library lib = new Library())
                {

                    Logger.Log("Library initialized ");

                    using (Document doc = new Document(finfo.FullName))
                    {
                        Logger.Log(String.Format("Opened PDF Document: {0}", finfo.Name));

                        try
                        {
                            using (PrintUserParams userParams = new PrintUserParams())
                            {
                                Logger.Log("set configuration");

                                // NOTE: userParams are only valid for ONE print job...
                                userParams.NCopies = NCopies;
                                userParams.ShrinkToFit = false;
                                userParams.PrintParams.ExpandToFit = false;
                               
                               // userParams.PortName

                                Logger.Log("Set InFileName");
                                userParams.InFileName = String.Format("{0} Test Print:SDK ", System.IO.Path.GetFileNameWithoutExtension(finfo.FullName));

                                userParams.UseDefaultPrinter(doc);

                                userParams.DeviceName = printer;

                                try
                                {
                                    //Console.WriteLine(String.Format("Printing (direct) to Printer: {0}", userParams.DeviceName));
                                    Logger.Log(String.Format("Printing (direct) to Printer: {0}", userParams.DeviceName));
                                    doc.Print(userParams, null /* for cancel see the PrintPDFGUI sample */, new SamplePrintProgressProc());
                                }
                                catch (Exception ex)
                                {
                                    Logger.Log("Error in printing: " + ex.Message + " " + ex.StackTrace);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            Logger.Log("Error on using pdf library " + ex.Message + " " + ex.StackTrace);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Log(ex.Message + " " + ex.StackTrace);
            }
        }
    }
}
